import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Rubiks {

    public Rubiks() {
        this(true);
    }

    // Creates a solved Rubiks cube
    public Rubiks(boolean fixCorner) {
        this.fixCorner = fixCorner;
        isTest = false;
        data = new int[Orientation.SIDES][Orientation.SIDELENGTH][Orientation.SIDELENGTH];
        for (int side = 0; side < Orientation.SIDES; side++) {
            for (int x = 0; x < Orientation.SIDELENGTH; x++) {
                for (int y = 0; y < Orientation.SIDELENGTH; y++) {
                    data[side][x][y] = side;
                }
            }
        }
    }

    @Override
    public String toString() {
        String myStr;
        StringBuilder str = new StringBuilder();

        // Face 0 (TOP)
        for (int x = 0; x < Orientation.SIDELENGTH; x++) {
            for (int i = 0; i < Orientation.SIDELENGTH; i++) {
                str.append("   ");
                if (isTest) {
                    str.append(" ");
                }
            }
            str.deleteCharAt(str.length() - 1);
            for (int y = 0; y < Orientation.SIDELENGTH; y++) {
                str.append(intToStr(data[0][Orientation.SIDELENGTH - 1 - x][y]));
                str.append(' ');
            }
            str.append('\n');
        }
        str.append('\n');

        // Face 1, 2, 5
        int[] faces = {5, 1, 2};
        for (int x = 0; x < Orientation.SIDELENGTH; x++) {
            for (int face : faces) {
                for (int y = 0; y < Orientation.SIDELENGTH; y++) {
                    switch (face) {
                        case 1:
                            str.append(intToStr(data[face][y][x]));
                            break;
                        case 2:
                        case 5:
                            str.append(intToStr(data[face][Orientation.SIDELENGTH - 1 - x][y]));
                            break;
                        default:
                            assert false;
                    }
                    str.append(' ');
                }
                str.append("  ");
            }
            str.append('\n');
        }
        str.append('\n');

        // Face 3
        for (int x = 0; x < Orientation.SIDELENGTH; x++) {
            for (int i = 0; i < Orientation.SIDELENGTH; i++) {
                str.append("   ");
                if (isTest) {
                    str.append(" ");
                }
            }
            str.deleteCharAt(str.length() - 1);
            for (int y = 0; y < Orientation.SIDELENGTH; y++) {
                str.append(intToStr(data[3][x][Orientation.SIDELENGTH - 1 - y]));
                str.append(' ');
            }
            str.append('\n');
        }
        str.append('\n');

        // Face 4
        for (int x = 0; x < Orientation.SIDELENGTH; x++) {
            for (int i = 0; i < Orientation.SIDELENGTH; i++) {
                str.append("   ");
                if (isTest) {
                    str.append(" ");
                }
            }
            str.deleteCharAt(str.length() - 1);
            for (int y = 0; y < Orientation.SIDELENGTH; y++) {
                str.append(intToStr(data[4][y][x]));
                str.append(' ');
            }
            str.append('\n');
        }
        str.append('\n');

        myStr = str.toString();
        return "Rubiks{" +
                "data=\n\n" + myStr +
                '}';
    }

    /* Commits a valid move
     * @arg axis Rotation around the TOP=0, FRONT=1 or RIGHT=2 axis (right hand rule, rotation vector facing in)
     * @arg level Which level to rotate, 0 at arrow beginning, SIDELENGTH-1 at arrow point
     * @arg rotation Between 1 and 3
     */
    public void move(final int axis, final int level, final int rotation) {
        assert axis < Orientation.AXIS && axis >= 0;
        assert level < Orientation.SIDELENGTH && level >= 0;
        assert rotation < Orientation.ROUNDTRIP && rotation > 0;

        // Check if move executed as told, or seperated in multiple moves (fixing a corner, or faces ...)
        if (wouldMoveInvariant(axis, level)) {
            moveKeepInvariant(axis, level, rotation);
            return;
        }

        commitMove(axis, level, rotation);
    }

    // Randomizes the cube
    public void randomize() {
        randomize(1000);
    }

    // Randomizes the cube
    public void randomize(int n) {
        for (int i = 0; i < n; i++) {
            int axis = ThreadLocalRandom.current().nextInt(0, Orientation.AXIS);
            int level = ThreadLocalRandom.current().nextInt(0, Orientation.SIDELENGTH);
            int rotation = ThreadLocalRandom.current().nextInt(1, Orientation.ROUNDTRIP);
            move(axis, level, rotation);
        }
    }

    public void deleteMoves() {
        moves.clear();
    }

    // Returns deep copy of cube
    public Rubiks copy() {
        Rubiks myClone = new Rubiks();
        myClone.data = new int[Orientation.SIDES][Orientation.SIDELENGTH][Orientation.SIDELENGTH];
        for (int i = 0; i < Orientation.SIDES; i++) {
            for (int j = 0; j < Orientation.SIDELENGTH; j++) {
                for (int k = 0; k < Orientation.SIDELENGTH; k++) {
                    myClone.data[i][j][k] = data[i][j][k];
                }
            }
        }
        myClone.isTest = isTest;
        myClone.fixCorner = fixCorner;
        // Moves are the same, but references to move stored in different ArrayList
        myClone.moves = (ArrayList<Move>) moves.clone();
        return myClone;
    }

    public Rubiks[] getNextRubiks() {
        Move[] availableMoves = getAvailableMoves();
        Rubiks[] availableRubiks = new Rubiks[availableMoves.length];
        for (int i = 0; i < availableMoves.length; i++) {
            availableRubiks[i] = copy();
            int axis = availableMoves[i].axis;
            int level = availableMoves[i].level;
            int rotation = availableMoves[i].rotation;
            availableRubiks[i].move(axis, level, rotation);
        }
        return availableRubiks;
    }

    public Move[] getAvailableMoves() {
        if (!availableMovesInitialized) {
            availableMovesInvariantCorner = new Move[Orientation.AXIS * (Orientation.SIDELENGTH - 1) * (Orientation.ROUNDTRIP - 1)];
            for (int axis = 0; axis < Orientation.AXIS; axis++) {
                for (int level = 0; level < Orientation.SIDELENGTH - 1; level++) {
                    for (int rotation = 1; rotation < Orientation.ROUNDTRIP; rotation++) {
                        Move currentMove;
                        if (axis == 2) {
                            currentMove = new Move(axis, level, rotation);
                            assert !wouldMoveInvariant(axis, level);
                        } else {
                            currentMove = new Move(axis, level + 1, rotation);
                            assert !wouldMoveInvariant(axis, level + 1);
                        }
                        availableMovesInvariantCorner[rotation - 1 + (Orientation.ROUNDTRIP - 1) * (level + (Orientation.SIDELENGTH - 1) * axis)] =
                                currentMove;
                    }
                }
            }
            availableMovesNoInvariant = new Move[Orientation.AXIS * (Orientation.SIDELENGTH) * (Orientation.ROUNDTRIP - 1)];
            for (int axis = 0; axis < Orientation.AXIS; axis++) {
                for (int level = 0; level < Orientation.SIDELENGTH; level++) {
                    for (int rotation = 1; rotation < Orientation.ROUNDTRIP; rotation++) {
                        Move currentMove;
                        currentMove = new Move(axis, level, rotation);
                        availableMovesNoInvariant[rotation - 1 + (Orientation.ROUNDTRIP - 1) * (level + Orientation.SIDELENGTH * axis)] =
                                currentMove;
                    }
                }
            }
        }
        availableMovesInitialized = true;
        return fixCorner ? availableMovesInvariantCorner : availableMovesNoInvariant;
    }

    private boolean wouldMoveInvariant(final int axis, final int level) {
        return fixCorner && tryMoveZero(axis, level);
    }

    private void moveKeepInvariant(final int axis, final int level, final int rotation) {
        assert wouldMoveInvariant(axis, level);

        switch (axis) {
            case 0:
            case 1:
                for (int i = 1; i < Orientation.SIDELENGTH; i++) {
                    move(axis, i, mod(-rotation, Orientation.ROUNDTRIP));
                }
                break;
            case 2:
                for (int i = 0; i < Orientation.SIDELENGTH - 1; i++) {
                    move(axis, i, mod(-rotation, Orientation.ROUNDTRIP));
                }
            default:
                assert false;
        }
    }

    private void commitMove(final int axis, final int level, final int rotation) {
        moves.add(new Move(axis, level, rotation));
        if (level == 0) {
            rotateFace(axis, rotation);
        } else if (level == Orientation.SIDELENGTH - 1) {
            rotateFace(axis + Orientation.OPPOSITE, mod(-rotation, Orientation.ROUNDTRIP));
        }
        rotateSide(axis, level, rotation);
    }

    private void rotateFace(final int axis, final int rotation) {
        assert axis < Orientation.AXIS && axis >= 0;
        assert rotation < Orientation.ROUNDTRIP && rotation > 0;

        for (int i = 0; i < rotation; i++) {
            rotateFaceOnce(axis);
        }
    }

    private void rotateFaceOnce(final int axis) {
        assert axis < Orientation.AXIS && axis >= 0;

        int[][] face = new int[Orientation.SIDELENGTH][Orientation.SIDELENGTH];
        for (int x = 0; x < Orientation.SIDELENGTH; x++) {
            for (int y = 0; y < Orientation.SIDELENGTH; y++) {
                face[x][y] = data[axis][y][Orientation.SIDELENGTH - 1 - x];
            }
        }
        data[axis] = face;
    }

    private void rotateSide(int axis, int level, int rotation) {
        assert axis < Orientation.AXIS && axis >= 0;
        assert level < Orientation.SIDELENGTH && level >= 0;
        assert rotation < Orientation.ROUNDTRIP && rotation > 0;

        // For geometry reasons, rotation around axis 1 is better done on virtual axis 4, (- axis 1)
        if (axis == 1) {
            axis = 4;
            level = Orientation.SIDELENGTH - 1 - level;
            rotation = mod(-rotation, Orientation.ROUNDTRIP);
        }
        for (int i = 0; i < rotation; i++) {
            rotateSideOnce(axis, level);
        }
    }

    private void rotateSideOnce(final int axis, final int level) {
        assert axis == 0 || axis == 2 || axis == 4;
        assert level < Orientation.SIDELENGTH && level >= 0;

        int[] side1 = new int[Orientation.SIDELENGTH];
        int[] side2 = new int[Orientation.SIDELENGTH];
        int[] side4 = new int[Orientation.SIDELENGTH];
        int[] side5 = new int[Orientation.SIDELENGTH];

        for(int i = 0; i < Orientation.SIDELENGTH; i++) {
            side1[i] = data[mod(axis+1, Orientation.SIDES)][i][level];
            side2[i] = data[mod(axis+2, Orientation.SIDES)][Orientation.SIDELENGTH - 1 - level][i];
            side4[i] = data[mod(axis+4, Orientation.SIDES)][Orientation.SIDELENGTH - 1 - i][Orientation.SIDELENGTH - 1 - level];
            side5[i] = data[mod(axis+5, Orientation.SIDES)][Orientation.SIDELENGTH - 1 - level][i];
            data[mod(axis+1, Orientation.SIDES)][i][level] = side2[i];
            data[mod(axis+2, Orientation.SIDES)][Orientation.SIDELENGTH - 1 - level][i] = side4[i];
            data[mod(axis+4, Orientation.SIDES)][Orientation.SIDELENGTH - 1 - i][Orientation.SIDELENGTH - 1 - level] = side5[i];
            data[mod(axis+5, Orientation.SIDES)][Orientation.SIDELENGTH - 1 - level][i] = side1[i];
        }
    }

    // Detects if 0 element would be moved
    private boolean tryMoveZero(final int axis, final int level) {
        return (axis == 0 && level == 0) || (axis == 1 && level == 0) || (axis == 2 && level == Orientation.SIDELENGTH - 1);
    }

    // Returns a mod b >= 0
    private int mod(final int a, final int b) {
        assert b > 0;

        return ((a % b) + b) % b;
    }

    private String intToStr(Integer a) {
        String str = a.toString();

        if (isTest && str.length() == 1) {
            str = "0" + str;
        }

        return str;

    }

    public class Move {

        Move(final int axis, final int level, final int rotation) {
            this.axis = axis;
            this.level = level;
            this.rotation = rotation;
        }

        public int axis;
        public int level;
        public int rotation;
    }

    protected int[][][] data;
    protected boolean isTest;
    protected boolean fixCorner;
    private ArrayList<Move> moves = new ArrayList<>();
    private static boolean availableMovesInitialized = false;
    private static Move[] availableMovesNoInvariant;
    private static Move[] availableMovesInvariantCorner;
}
