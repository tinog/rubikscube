import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void test() {
        TestRubiks cube = new TestRubiks(true, TestRubiks.TestCases.ALL_DIFFERENT_COLORS);
        System.out.println(cube);
        cube.randomize();
        System.out.println(cube);
    }

    public static void main(String[] args) {
        test();
    }
}
