import com.sun.org.apache.xpath.internal.operations.Or;

public class TestRubiks extends Rubiks {

    public enum TestCases{
        DISPLAY_ORIENTATION,
        ALL_DIFFERENT_COLORS
    }

    public TestRubiks(boolean fixCorner, TestCases testCase) {
        this.fixCorner = fixCorner;
        isTest = true;
        this.testCase = testCase;
        if (testCase == TestCases.DISPLAY_ORIENTATION || testCase == TestCases.ALL_DIFFERENT_COLORS) {
            int differentColors = (testCase == TestCases.ALL_DIFFERENT_COLORS) ? 9 : 0;
            data = new int[Orientation.SIDES][Orientation.SIDELENGTH][Orientation.SIDELENGTH];
            for (int side = 0; side < Orientation.SIDES; side++) {
                for (int x = 0; x < Orientation.SIDELENGTH; x++) {
                    for (int y = 0; y < Orientation.SIDELENGTH; y++) {
                        data[side][x][y] = differentColors * side + Orientation.SIDELENGTH * x + y;
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Test" + super.toString();
    }

    public void assertions() {
        if (testCase == TestCases.ALL_DIFFERENT_COLORS) {
            for (int i = 0; i < Orientation.SIDELENGTH * Orientation.SIDELENGTH * 6; i++) {
                assert foundTimes(i) == 1;
            }
        } else {
            for (int i = 0; i < Orientation.SIDELENGTH * Orientation.SIDELENGTH; i++) {
                assert foundTimes(i) == Orientation.SIDES;
            }
        }
        assert !fixCorner || data[0][0][0] == 0;
    }

    private int foundTimes(final int color) {
        int i = 0;
        for (int[][] c2 : data) {
            for (int[] c1 : c2) {
                for (int c : c1) {
                    if (c == color) i++;
                }
            }
        }
        return i;
    }

    private TestCases testCase;

}
