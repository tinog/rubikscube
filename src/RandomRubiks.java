public class RandomRubiks extends Rubiks {

    RandomRubiks() {
        this(true);
    }

    RandomRubiks(boolean fixCorner) {
        super(fixCorner);
        randomize();
        deleteMoves();
    }
}
