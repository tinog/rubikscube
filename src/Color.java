public class Color {
    public static final int RED = 0;
    public static final int BLUE = 1;
    public static final int WHITE = 2;
    public static final int ORANGE = 3;
    public static final int GREEN = 4;
    public static final int YELLOW = 5;
}
