public class Orientation {
    public static final int TOP = 0;
    public static final int FRONT = 1;
    public static final int RIGHT = 2;
    public static final int BOTTOM = 3;
    public static final int BACK = 4;
    public static final int LEFT = 5;
    public static final int SIDES = 6; // Given by 3d geometry
    public static final int SIDELENGTH = 3; // Code so that changing this makes bigger cube
    public static final int AXIS = 3; // TOP, FRONT and RIGHT axis
    public static final int ROUNDTRIP = 4; // Turn four times to have the same cube
    public static final int OPPOSITE = 3; // The opposite side is 3 mod SIDES away
}
